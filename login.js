 'use strict';

(function() {
    var formLogin = document.getElementById('form-login');
    formLogin.addEventListener('submit',login);

    /**
     * Calls login WS in sanvipop
     * @param {Event} e
     */
    function login(e){
        e.preventDefault();

        var email = document.getElementById('correo').value;
        var password = document.getElementById('password').value;

        var http = new XMLHttpRequest();
        http.open('POST', '/sanvipop/login', true);
        http.setRequestHeader("Content-type", "application/json");
        var request = JSON.stringify({"correo" : email, "password" : password});
        var errorResponse = document.getElementById('errorInfo');
        http.send(request); 
        http.addEventListener('readystatechange', function() { 
            if(http.readyState === 4 && http.status === 200) {
                var response = JSON.parse(http.responseText); 
                if(response.ok) {
                   window.location = "/sanvipop-client/products.html";
                } else {
                    errorResponse.textContent = 'Login failed';
                }
            }
        });
    }


})();