'use strict';

(function() {
    loadCategories();
    
    var imgData = '';
    loadImage();

    var formNewProduct = document.getElementById('form-addprod');
    formNewProduct.addEventListener('submit', newProduct);
    
    /**
     * It gets all input from the form, then it validates and if all it is valid
     * it sends the product to the WS sanvipop
     * @param {Event} e
     */
    function newProduct(e){
        e.preventDefault();

        var title = document.getElementById('titulo').value;
        var description = document.getElementById('descripcion').value;
        var price = document.getElementById('importe').value;
        var category = document.getElementById('categoria').value;
        var img = imgData;

        if(formIsValid(title, description, price, category, img)){
            var request = JSON.stringify(
            {
                'titulo' : title, 
                "descripcion" : description, 
                "categoria" : category,
                "importe" : price,
                "imagen" : img
            });
            addProduct(request);
        } 
    }

    /**
     * Check if the form is valid. If it is not valid it 
     * updates errorInfo <p> element.
     * @param {String} title
     * @param {String} description
     * @param {String} price
     * @param {String} category
     * @param {String} img
     * @return true if all inputs are correc if they are not correct
     */
    function formIsValid(title, description, price, category, img){
        var errorResponse = document.getElementById('errorInfo');
        errorResponse.textContent = '';

        if(img.trim().length === 0)  
            errorResponse.textContent = 'Image cannot be empty';
        if(price.trim().length === 0)  
            errorResponse.textContent = 'Price cannot be empty';
        if(description.trim().length === 0)  
            errorResponse.textContent = 'Description cannot be empty';
        if(title.trim().length === 0) 
            errorResponse.textContent = 'Title cannot be empty';
        
        return errorResponse.textContent.length === 0;
    }

    /**
     * Sends a product to add to sanvipop ws
     * @param {JSON} request 
     * 
     */
    function addProduct(request){
        var http = new XMLHttpRequest();
        http.open('POST', '/sanvipop/productos', true);
        http.setRequestHeader("Content-type", "application/json");
        
        var errorResponse = document.getElementById('errorInfo');

        http.send(request); 

        http.addEventListener('readystatechange', function() { 
            if(http.readyState === 4 && http.status === 200) {
                var response = JSON.parse(http.responseText); 
                if(response.ok) {
                    window.location = "/sanvipop-client/products.html";
                } else {
                    errorResponse.textContent =  response.error;
                }
                    
            }
        });
    }

    /**
     * Load all categories from sanvipop WS
     */
    function loadCategories(){
        var http = new XMLHttpRequest();
        http.open('GET', '/sanvipop/categorias', true);
        http.send();
        http.addEventListener('readystatechange', function() {
            if(http.readyState === 4 && http.status === 200) {
                var categoriasJson = JSON.parse(http.responseText);
                var selectCategorias = document.getElementById('categoria');
                for(var i = 0; i < categoriasJson.length; i++){
                    var optionCategoria = document.createElement('option');
                    optionCategoria.value = categoriasJson[i].id;
                    optionCategoria.textContent = categoriasJson[i].descripcion;
                    selectCategorias.appendChild(optionCategoria);
                }

            } 
         });
    }

    /**
     * Load the image from the input 
     */
    function loadImage(){
        var imgInput = document.getElementById('image');
    
        imgInput.addEventListener('change', function () { 
            var  file = imgInput.files[0];
            var reader = new FileReader();
            reader.addEventListener("load", function () { 
                var imgPreview = document.getElementById('image-preview');
                imgPreview.src = imgData = reader.result;
            }, false);

            if (file) { 
                reader.readAsDataURL(file);
            } 
        });
    }

})();
