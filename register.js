 'use strict';
 
(function() {
    var ERROR_GEOLOCAZING = "Error geolocalizing...";
    var DEFAULT_LONGITUDE = '-0.1234';
    var DEFAULT_LATITUDE = '38.1425';
    var isGeologalizated = false;

    loadGeolocation();

    var formRegister = document.getElementById('form-register');
    formRegister.addEventListener('submit', register);

    /**
     * It registers an user if all input are valids
     * @param {Event} e
     */
    function register(e){
        e.preventDefault();

        var name = document.getElementById('nombre').value;
        var email = document.getElementById('correo').value;
        var password = document.getElementById('password').value;
        var password2 = document.getElementById('password2').value;
        var latitude = document.getElementById('latitude').value;
        var longitude = document.getElementById('longitude').value;

        latitude = latitude === ERROR_GEOLOCAZING ? DEFAULT_LATITUDE : latitude;
        longitude = longitude === ERROR_GEOLOCAZING ? DEFAULT_LONGITUDE : longitude;

        var errorResponse = document.getElementById('errorInfo');
        errorResponse.textContent = '';
        if(!validatePasswords(password, password2))
            errorResponse.textContent = 'Both passwords must match and needs to be at least 4 characters long.';
        if(!validateEmail(email))        
            errorResponse.textContent = 'It is not a valid email, it must not be empty and be like user@mail.es a';
        if(!validateValueIsNotBlank(name)) 
            errorResponse.textContent = 'User name cannot be empty';
        if(!isGeologalizated)
            errorResponse.textContent = 'Not geolocalizated yet';
            
        if(errorResponse.textContent.length === 0){
            var request = JSON.stringify(
                {
                    'nombre' : name, 
                    "correo" : email, 
                    "password" : password,
                    "lat" : latitude,
                    "lng" : longitude
                });

            registerUser(request);
        } 
    }

    /**
     * Validates the value is not empty or blank
     * @param {String} value
     * @return true if is not blank and false if it is
     */
    function validateValueIsNotBlank(value){
        return value.trim().length > 0;
    }

    /**
     * Validates password are equals
     * @param {String} password1
     * @param {String} password2
     * @return value of error if the passwords don't match or empty string if
     * are equals
     */
    function validatePasswords(password1, password2){
        return password1 === password2 && password1.length > 3 && password2 > 3;
    }

    /**
     * It validates the email matches the Regex /\w+(\.\w+)*@\w+(\.\w+)+/
     * @param {String} email 
     * @return value of error if the email don't match the Regex
     */
    function validateEmail(email){
        var regexEmail = /\w+(\.\w+)*@\w+(\.\w+)+/;
        return regexEmail.test(email) && email.trim().length > 0 ;       
    }

    /**
     * It sends the user info to register a new user
     * in sanvipop WS
     * @param {JSON} request
     */
    function registerUser(request){
        var http = new XMLHttpRequest();
        http.open('POST', '/sanvipop/registro', true);
        http.setRequestHeader("Content-type", "application/json");

        var errorResponse = document.getElementById('errorInfo');

        http.send(request); 

        http.addEventListener('readystatechange', function() { 
            if(http.readyState === 4 && http.status === 200) {
                var response = JSON.parse(http.responseText); 
                if(response.ok) 
                    window.location = "/sanvipop-client/products.html";
                else {
                    errorResponse.textContent =  response.error;
                }
                    
            }
        });
    }

    /**
     * It geolocalizates the user and loads it into inputs. If there is an error it loads error message.
     */
    function loadGeolocation(){
        navigator.geolocation.getCurrentPosition(
            function(pos) {
                document.getElementById('longitude').value = pos.coords.longitude;
                document.getElementById('latitude').value = pos.coords.latitude;

                isGeologalizated = true;
            }, 
            function(error) {
                document.getElementById('longitude').value = ERROR_GEOLOCAZING;
                document.getElementById('latitude').value = ERROR_GEOLOCAZING;
                var errorText = document.getElementById('errorInfo');
                switch(error.code) {
                    case error.PERMISSION_DENIED: 
                        errorText.textContent = "You have denied the request for Geolocation.";
                        break;
                    case error.POSITION_UNAVAILABLE: 
                        errorText.textContent = "Location information is unavailable.";
                        break;
                    case error.TIMEOUT: 
                        errorText.textContent = "The request to get user location timed out.";
                        break;
                    case error.UNKNOWN_ERROR:
                        errorText.textContent = "An unknown error occurred." ;
                        break;
                }

                isGeologalizated = true; 
            }
        );  
    }


})();