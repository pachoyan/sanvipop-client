'use strict';

(function() {
    
    var id = getParam('id');
    loadProductDetail(id);

    /**
     * Load details of the product and put it into the html elements
     * @param {Number} id of the product
     */
    function loadProductDetail(id){
        var http = new XMLHttpRequest();
        http.open('GET', ' /sanvipop/productos/' + id, true);
        http.send();
        http.addEventListener('readystatechange', function() {
            if(http.readyState === 4 && http.status === 200) {
                var productJson = JSON.parse(http.responseText);

                var title = document.getElementById('title');
                var photo = document.getElementById('photo');
                var description = document.getElementById('description');
                var price = document.getElementById('price');
                var soldBy = document.getElementById('sold-by');
                var category = document.getElementById('category');
                var date = document.getElementById('date');
                var numVisits = document.getElementById('num-visits');

                title.textContent = productJson.titulo;  
                photo.src = productJson.imagenPrincipal;
                description.textContent = productJson.descripcion;
                price.textContent = 'Price: ' + productJson.importe + ' €';
                soldBy.textContent = 'Sold by: ' + productJson.nombreUsuario;
                category.textContent = productJson.categoria;
                date.textContent = productJson.fechaPublicado;
                numVisits.textContent = productJson.numVisitas; 

                loadStaticMap(productJson.lat, productJson.lng);
                initMap(productJson.lat, productJson.lng);
            }
        });
    }

    /**
     * Load static map to iamge
     * @param {String} latitude
     * @param {String} longitude
     */
    function loadStaticMap(latitude, longitude){
        var latlon = latitude + "," + longitude;
        var imgUrl = "https://maps.googleapis.com/maps/api/staticmap?center="
            + latlon + "&zoom=14&size=400x300&sensor=false&&markers=color:red%7Clabel:C%7C" + latlon; 
        document.getElementById("map").setAttribute('src', imgUrl);
    }

    /**
     * Get the value of the request param
     * @param {String} name
     * @return {String} the value of the request param
     */
    function getParam(name){
        if(name=(new RegExp('[?&]'+encodeURIComponent(name)+'=([^&]*)')).exec(location.search))
        return decodeURIComponent(name[1]);
    }

    /**
     * Inist map in position of latitude and longitude
     * @param {String} latitude
     * @param {String} longitude
     */
    function initMap(latitude, longitude) {
        var map = new google.maps.Map(document.getElementById('map-canvas'), {
          center: {lat: latitude, lng: longitude},
          zoom: 8
        });

        var marker = new google.maps.Marker({
            position: {lat: latitude, lng: longitude},
            map: map,
            title: 'Location where is sold'
        });
      }

})();


