'use strict';

(function() {
    
    var buttonLogout = document.getElementById('logout');
    buttonLogout.addEventListener('click', logout);

    /**
     * It calls to sanvipop WS to logout
     * @param {Event} e
     */
    function logout(e){ 
        e.preventDefault();
        var http = new XMLHttpRequest();
        http.open('GET', '/sanvipop/logout', true);
        http.send();
        http.addEventListener('readystatechange', function() { 
            if(http.readyState === 4 && http.status === 200) {
                var response = JSON.parse(http.responseText); 
                if(response.ok) {
                    window.location = "/sanvipop-client/index.html";
                }         
            }
        });

    }
    
})();