'use strict';

(function() {
    var DEFAULT_LONGITUDE = '-0.1234';
    var DEFAULT_LATITUDE = '38.1425';
    
    loadProducts();

    /**
     * Load products from sanvipop WS and 
     * create html elements
     */
    function loadProducts(){
        navigator.geolocation.getCurrentPosition(
            function(pos) {
                getProcutsFromServer(pos.coords.latitude, pos.coords.longitude);
            }, 
            function(error) {
                getProcutsFromServer(DEFAULT_LATITUDE, DEFAULT_LONGITUDE);
            }
        );
    }

    /**
     * Gets the product from the server
     * @param {String} latitude
     * @param {String} longitude
     */
    function getProcutsFromServer(latitude, longitude){
        var http = new XMLHttpRequest();
        http.open('GET', '/sanvipop/productos/' + latitude + '/' + longitude, true);
        http.send();
        http.addEventListener('readystatechange', function() {
            if(http.readyState === 4 && http.status === 200) {
                var productsJson = JSON.parse(http.responseText);
                var listProductsDiv = document.getElementById('list-products');
                for(var i = 0; i < productsJson.length; i++){
                    listProductsDiv.appendChild(createDivColumn(productsJson[i]));
                }
                
            } 
        });
    }
    
    /**
     * Create node div column to append to list product div
     * @param {Json} product
     * @return {Html} html element div column
     */
    function createDivColumn(product){
        var divCol = document.createElement('div');
        divCol.className = 'col-md-6 product';

        divCol.appendChild(createTitle(product));
        divCol.appendChild(createBody(product));

        return divCol;
    }

    /**
     * Create node title to append to div col
     * @param {JSON} product
     * @return {Html} html element div title
     */
    function createTitle(product){
        var divTitle = document.createElement('div');
        divTitle.className = 'product-title';

        var titleLink = document.createElement('a');
        titleLink.setAttribute('href', 'product-detail.html?id=' + product.id);
        titleLink.textContent = product.titulo;

        divTitle.appendChild(titleLink);

        return divTitle;  
    }

    /**
     * Create node body and append to div col
     * @param {JSON} product
     * @return {Html} html element div body
     */
    function createBody(product){
        var divBody = document.createElement('div');
        divBody.className = 'product-body';
        
        divBody.appendChild(createImg(product.imagenPrincipal));
        divBody.appendChild(createDescription(product.descripcion));
        divBody.appendChild(createPrice(product.importe));
        divBody.appendChild(createDistance(product.distancia));
        divBody.appendChild(createSoldBy(product.nombreUsuario));
        divBody.appendChild(createClearFix());

        return divBody;
    }

    /**
     * Create image html element
     * @param {String} value
     * @return the image html element
     */
    function createImg(value){
        var prodImg = document.createElement('img');
        prodImg.setAttribute('src', value);
        prodImg.setAttribute('alt', '');
        return prodImg;
    }

    /**
     * Create description html element
     * @param {String} value
     * @return the description html element
     */
    function createDescription(value){
        var descriptionText = document.createElement('p');
        descriptionText.textContent = value;
        return descriptionText;
    }

    /**
     * Create price html element
     * @param {String} value
     * @return the price html element
     */
    function createPrice(value){
        var priceText = document.createElement('p');
        priceText.className = 'product-outline';
        priceText.textContent = 'Price: ' + value + ' €';
        return priceText;
    }

    /**
     * Create distance html element
     * @param {String} value
     * @return the distance html element
     */
    function createDistance(value){
        var distanceText = document.createElement('p');
        distanceText.className = 'product-outline';
        distanceText.textContent = 'Distance: ' + value + ' km';
        return distanceText;
    }

    /**
     * Create soldby html element
     * @param {String} value
     * @return the soldy html element
     */
    function createSoldBy(value){
        //<p class="product-outline">Sold by: Pedro Pérez</p>
        var soldByText = document.createElement('p');
        soldByText.className = '';
        soldByText.textContent = 'Sold by: ' + value;
        return soldByText;
    }

    /**
     * Create clear fix html element
     * @return clear fix html element
     */
    function createClearFix(){
        var divClearFix = document.createElement('div');
        divClearFix.className = 'clearfix';
        return divClearFix;
    }


})();